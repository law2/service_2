from django.urls import path
from .views import *
from django.conf import settings
from django.conf.urls import static
urlpatterns = [
    path('',  index, name="index"),
    path('all-file/', fetch_all, name="read_file")
]