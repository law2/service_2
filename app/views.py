from django.shortcuts import render, redirect
from .forms import *
# Create your views here.



def index(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/')
    else:
        form = DocumentForm()
    return render(request, 'file_handler/index.html', {'form': form})


def fetch_all(request):
    all_files = Document.objects.all()
    counter = all_files.count()
    files = (all_files, counter)
    return render(request, 'all_file.html', {'files': all_files})