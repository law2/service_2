from django.forms import ModelForm
from django import forms
from .models import Document

class DocumentForm(ModelForm):
    class Meta:
        model = Document
        fields = ('name','description', 'document')
        widgets = {
            'name': forms.TextInput(
				attrs={
					'class': 'form-control'
					}
				),
            'description': forms.TextInput(
				attrs={
					'class': 'form-control'
					}
				),
            'document': forms.FileInput(
                attrs={
					'class': 'form-control'
					} 
                )
            }